<!DOCTYPE html>
<!-- USE MID PAGE COMMENT AREA FOR OUTAGES -->
<?php
	/****************************************************************
	* file: index.php                                    		    *
	* func: Home page for Electrical Continuing Education           *
	* date: 9/26/2013                                               *
	* auth: Justin Brylski                             			    *
	* mods: New banners MT & FL 5.6.2016                            *
	* 10/02/2015 - added base url variable 						    *
	****************************************************************/
	session_start();
	$debug = 0;

	// Get the user, if there. If not assign a 'not present' value

	if (!empty($_SESSION['sessuser'])) 	$username = $_SESSION['sessuser'];
	else $username = "-1";

	// For passed in AdWords camps
	if(empty($_SESSION['campID']) && !empty($_GET['gjcid'])) 	// If empty, check out URL
		$_SESSION['campID'] = $_GET['gjcid'];
	if(empty($_SESSION['googID']) && !empty($_GET['gclid']))	// If empty, check out URL
		$_SESSION['googID'] = $_GET['gclid'];

	// For other passed in referrals
	if(!empty($_GET['passreff'])) $_SESSION['passreff'] = $_GET['passreff'];
	if(empty($_SESSION['campID']) && !empty($_GET['a'])) 	// If empty, check out URL
		$_SESSION['campID'] = $_GET['a'];

	// Environment variables
	include_once("labolg/glconnect.php");

	// Captcha stuff
	require_once 'jadecc/mathcaptcha.class.php';
	$captcha = new MathCaptcha();

	// PORTAL MASTER CONTROL VARIABLEi
	$_SESSION['PORTAL'] = "jade";

	// If this is their 4th+ error on login try, send them directly to the login help page
	if(isset($_SESSION['errorCnt']) && $_SESSION['errorCnt'] > 3)
		header("Location: https://".JADE1_URL."/jadecc/accountRecoveryForm.php");
	if(empty($_SESSION['errorCnt']))
		$_SESSION['errorCnt'] = 0;

	$dbLink = new mysqli($DBhost, $DBuser, $DBpass, JADECC_DB);
	if($dbLink)
	{
		// If they are logged in, give them some credit for poking around, ignore error
		include("jadecc/timestampi.inc");

		// See how many active courses we have right now - TOTAL
		$activeC_nowTQ  = "SELECT Course_ID FROM Course_Attributes_tbl WHERE active = '1'";
		$activeC_nowTQ .= " AND (stateOG IS NULL OR stateOG = '')";
		$activeC_nowTRS = $dbLink->query($activeC_nowTQ);
		$activeC_nowTCt = $activeC_nowTRS->num_rows;
		$ceCourseCountT = $activeC_nowTCt;

		// See how many active courses we have right now
		$activeC_nowQ  = "SELECT Course_ID FROM Course_Attributes_tbl WHERE active = '1'";
		$activeC_nowQ .= " AND (Trade_ID = 'Electrical' || Trade_ID = 'AKjourn' || Trade_ID = 'AKadmin' || Trade_ID = 'Apprentice')";
		$activeC_nowQ .= " AND (stateOG IS NULL OR stateOG = '')";
		$activeC_nowRS = $dbLink->query($activeC_nowQ);
		$activeC_nowCt = $activeC_nowRS->num_rows;
		$ceCourseCount = $activeC_nowCt;

		// See how many boards we are approved with right now
		$activeST_nowQ  = "SELECT State_ID, Trade_ID FROM State_Trade_Info_tbl WHERE active = '1'";
		$activeST_nowQ .= " AND (Trade_ID = 'Electrical' || Trade_ID = 'AKjourn' || Trade_ID = 'AKadmin' || Trade_ID = 'Apprentice')";
		$activeST_nowRS = $dbLink->query($activeST_nowQ);
		$activeST_nowCt = $activeST_nowRS->num_rows;

		// See how states we are approved with right now
		$activeS_nowQ  = "SELECT DISTINCT State_ID FROM State_Trade_Info_tbl WHERE active = '1'";
		$activeS_nowQ .= " AND (Trade_ID = 'Electrical' || Trade_ID = 'AKjourn' || Trade_ID = 'AKadmin' || Trade_ID = 'Apprentice')";
		$activeS_nowRS = $dbLink->query($activeS_nowQ);
		$activeS_nowCt = $activeS_nowRS->num_rows;

		// Get events 1 and 2
		$eventsQ  = "SELECT * FROM ClassSeats_tbl";
		$eventsQ .= " WHERE active = '1'";
		$eventsQ .= " AND seatsLeft > '0'";
		$eventsQ .= " AND (classDate > CURDATE() and classDate < (CURDATE() + interval 3 month))";
		$eventsQ .= " ORDER BY classDate";
		$eventsRS = $dbLink->query($eventsQ);
		if($debug) echo "<b>eventsQ: </b>".$eventsQ."<br>\n";
		if($dbLink->errno) echo ( "<p>mysql error: " . mysqli_error($dbLink) . "</p>\n" );
		$eventsCt = $eventsRS->num_rows;
		$eventsShowing = 0;
		$eventShowCount = 2;
		$eventList = "";
		$classNames = "";
		$classLocation = "";
		$thisEventClassIDCore = "";
		$nextEventClassIDCore = "";
		while($eventsRW = $eventsRS->fetch_assoc())
		{
			// Max events specified max
			if($eventsShowing <= $eventShowCount)
			{
				// Coming up here we have...
				$nextEventClassIDCore = substr($eventsRW['classCourseID'], 0, strrpos($eventsRW['classCourseID'], "_"));
				if($debug) echo"<b>nextEventClassIDCore: </b>".$nextEventClassIDCore."<br>\n";

				// Not the same as the current?
				if($nextEventClassIDCore != $thisEventClassIDCore)
				{
					// For successive LIs wrap them up before starting a new one
					if(!empty($thisEventClassIDCore))
					{
						// End of LI
						$eventList .= "   <h4>".$classNames."</h4>\n";
						$eventList .= "   <p>".$classLocation."<br>8:00am - 5:00pm</p>\n";
						$eventList .= "</li>\n";
						$eventList .= "</a>\n";
					}

					if($eventsShowing == $eventShowCount)
						break;

					// Induct the new class core ID
					$thisEventClassIDCore = substr($eventsRW['classCourseID'], 0, strrpos($eventsRW['classCourseID'], "_"));
					if($debug) echo"<b>Inducted: thisEventClassIDCore: </b>".$thisEventClassIDCore."<br><br>\n";

					// Beginning of LI
					$eventList .= "<a href='https://".JADE1_URL."/jadecc/payment/classroomSelector.php' title='NC Electrical Continuing Education Classroom Courses Signup'>\n";
					$eventList .= "<li>\n";
					$eventList .= "   <p class='date'><span>".date("j", strtotime($eventsRW['classDate']))."</span><span>".date("M", strtotime($eventsRW['classDate']))."</span></p>\n";

					// Names and location
					$classNames = "";
					$classNames .= $eventsRW['className'];
					$classLocation = "";
					$classLocation = $eventsRW['classLocation'];

					// We got one for showing
					$eventsShowing++;
				}
				else
					$classNames .= ", ".$eventsRW['className'];
			}
			else
			{
				// End of LI
				$eventList .= "   <h4>".$classNames."</h4>\n";
				$eventList .= "   <p>".$classLocation."<br>8:00am - 5:00pm</p>\n";
				$eventList .= "</li>\n";
				$eventList .= "</a>\n";
				break;
			}
		}

		$dbLink->close();
	}
	else
	{
		$ceCourseCount = "tons of";
		$activeST_nowCt = "multiple";
		$activeS_nowCt = "most";
	}
?>
<html lang="en"><head>
    <meta charset="UTF-8">
    <title>Electrical Continuing Education</title>
    <meta name="description"  content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Signika:600,400,300' rel='stylesheet' type='text/css'>
    <link href="//<?=JADE1_URL ?>/css/LiteTooltip.min.css" rel="stylesheet" />
    <link href="//<?=JADE1_URL ?>/newsletter/assets/css/quick_newsletter.css" rel="stylesheet" media="screen">
    <link href="//<?=JADE1_URL ?>/css/new-styles.css?v=<?= rand(100, 99999); ?>" rel="stylesheet" /> <!-- added for seo a/b testing -->

	<?php
		//initiallize a few variables to prevent the logs from getting overrun with warnings
		$req_accss = '';
		$ses_accss = '';

		if(isset($_REQUEST['accss']))
			$req_accss = $_REQUEST['accss'];

		if(isset($_SESSION['accss']))
			$ses_accss = $_SESSION['accss'];

	// If the access is coming from the app or already established byt he app, create a similar navigational experience
	if($req_accss == "app" || $ses_accss == "app")
	{
		$_SESSION['accss'] = "app";
		$_SESSION['appSite'] = "ece";
		$_SESSION['cju'] = $_REQUEST['cju'];	// Current JADE user?
		$_SESSION['jun'] = $_REQUEST['jun'];	// Current JADE username?
		$_SESSION['aUDID'] = $_REQUEST['aUDID'];// Unique device ID

		// If we have both of the pieces of data needed for an app to RE-authenticate, then do it. JUN only set after 1 auth on that device
		if(!empty($_SESSION['jun']) && !empty($_SESSION['aUDID']) && $_REQUEST['appAlreadyAuthed'] != "didAuth")
			header("Location: //".JADE1_URL."/jadecc/accounts/logcheck.php?onto=jade&accss=app");
		?>
        <meta name="viewport" content="initial-scale=1.0,user-scalable=no,maximum-scale=1">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<link href='//fonts.googleapis.com/css?family=Signika:600,400,300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" media="all" href="//<?=JADE1_URL ?>/sharedAssets/webslidemenu/css/webslidemenu.css" />
		<link rel="stylesheet" href="//<?=JADE1_URL ?>/sharedAssets/webslidemenu/font-awesome/css/font-awesome.min.css" />
        <?php
		echo "<link href='//".JADE1_URL."/css/newstyleDevices.css' rel='stylesheet' type='text/css' media='screen'>\n";
	}
	else
		echo "<link href='//".JADE1_URL."/css/newstyle.css' rel='stylesheet' type='text/css' media='screen'>\n";
	?>

    <!-- Web icons for bookmarking on devices -->
    <link rel="apple-touch-icon" sizes="60x60" href="//<?=JADE1_URL ?>/images/webIcons/webIcon60.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="//<?=JADE1_URL ?>/images/webIcons/webIcon76.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="//<?=JADE1_URL ?>/images/webIcons/webIcon120.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="//<?=JADE1_URL ?>/images/webIcons/webIcon152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="//<?=JADE1_URL ?>/images/webIcons/webIcon180.png" />

    <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!-- input field placeholder non jQuery polyfill -->
    <script type="text/javascript" src="//<?=JADE1_URL ?>/jadecc/courses/UNIVERSAL/js/Placeholders.js"></script>
    <script src="//<?=JADE1_URL ?>/loginJazzNew.js" type="text/javascript"></script>
	<script src="//<?=JADE1_URL ?>/jadecc/accounts/profile/assets/js/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script src="//<?=JADE1_URL ?>/newsletter/assets/scripts/quick_newsletter.js" type="text/javascript"></script>
    <!-- Pingdom RUM Monitoring -->
    <script type="application/javascript">var _prum={id:"5166fa98e6e53d3861000000"};var PRUM_EPISODES=PRUM_EPISODES||{};PRUM_EPISODES.q=[];PRUM_EPISODES.mark=function(b,a){PRUM_EPISODES.q.push(["mark",b,a||new Date().getTime()])};PRUM_EPISODES.measure=function(b,a,b){PRUM_EPISODES.q.push(["measure",b,a,b||new Date().getTime()])};PRUM_EPISODES.done=function(a){PRUM_EPISODES.q.push(["done",a])};PRUM_EPISODES.mark("firstbyte");(function(){var b=document.getElementsByTagName("script")[0];var a=document.createElement("script");a.type="text/javascript";a.async=true;a.charset="UTF-8";a.src="//rum-static.pingdom.net/prum.min.js";b.parentNode.insertBefore(a,b)})();</script>
			<?php include_once($_SERVER["JADEROOT"]."/analyticstracking.php") ?>
	<?php require("jadecc/headers/state-specific-handling.php"); ?>

</head>

<body class="home boxed shadow">
<?php

var_dump('This is a test');
?>
    <!-- If the access is coming from the app, create a similar navigational experience -->
	<? if($ses_accss =="app") include("sharedAssets/webslidemenu/appSiteMenuWrapTopECE.inc.php"); ?>

	<div class="root">
	<?php require("jadecc/headers/navigation.php"); ?>
	<section class="home-hero">
	    <div class="container">
	        <div class="text-container left wide text-left">
	            <h2 class="text-left hero-title">Renew Your Electrical License</h2>
	            <ul class="text-left hero-list">
	                <li>Online courses available 24/7 from anywhere</li>
	                <li>Taught by licensed electricians and NEC code experts</li>
	                <li>We inform your state you completed your education</li>
	                <li>You pay only after you pass</li>
	            </ul>
	            <a href="//<?=JADE1_URL; ?>/jadecc/registration/register.php" title="All of Our Continuing Education Courses" class="button left hero-start-for-free">Start For Free</a>
	            <span class="sub-notice left">Pay only when you pass.</span>
	        </div>
	        <img src="images/home-hero-mobile.jpg" class="mobile">

	    </div>
		<div class="lower-container second-info-bar" style="display: none;">
			<a href="//<?=JADE1_URL; ?>/jadecc/view_courses.php">
				<span class="green-bar-text">Beat the deadline: Renew your electrical license now</span>
			</a>
		</div>
		<div class="lower-container">
			<span class="green-bar-text">Beat the deadline: Renew your electrical license now</span>
			<a href="//<?=JADE1_URL; ?>/jadecc/view_courses.php" class="button green-bar-button">View All Courses</a>
		</div>

	</section>

	<section class="content">
        <section class="main" style="max-width:700px;">
			<article class="intro">
				<!--
                <h5 style="background-color:#FF0; border:2px solid red; padding:5px;">
                    <font color='red'>*Website Maintenance Scheduled for Thursday October 9th, 2014<br></font>
                    <br>Our website will be inaccessible for a little bit around up until 12 noon Eastern Time for scheduled maintenance.
                    We will be available by phone after 7:30 am. Its taking a little longer than usual!
                    <br><br> We will be back online as soon as possible. Thanks for your patience.
                </h5>
                -->
								<h1>Electrical Continuing Education – Renew Your Electrical License Now</h1>
								<section class="hp-intro">
										<p class="slogan"><b>100% Free To Try Everything</b> Get a Free Account Now</p>
										<p class="cta"><a href="//<?=JADE1_URL ?>/jadecc/registration/register.php" title="Register for Continuing Education Courses" class="button">Start Here!</a></p>
								</section>
				<p>
                	Our continuing education courses are taught by licensed electricians and NEC code experts with decades of experience.  Renew your electrical license or electrical certificate with continuing education courses approved by <?=$activeST_nowCt?> boards in <?=$activeS_nowCt?> states.
 				</p>
			</article>



			<section class="noPicBorder">
				<h2 style="margin-top:0; margin-bottom:0;"><span>What Do We Offer?</span></h2>
                <article>
					<a href="//<?=JADE1_URL ?>/jadecc/demos/JADE_Demo_NewSite/index.html" title="View A Course How-To Tutorial" target="_blank">
                    	<div class="alignleft"><img src="//<?=JADE1_URL ?>/images/instructionalVideo2.png" alt="View Course How-To Tutorial"></div>
						<h3>Course How-To Tutorial</h3>
						<p>
                        	This short video shows you how to login to your account and work on a course. Highlighted features
                            include our See What Others Are Saying (SWOSS) system and immediate feedback.
                      	</p>
                 	</a>
				</article>
				<article>
					<a href="//<?=JADE1_URL ?>/jadecc/demos/CourseCatalog2015.pdf" title="Download Our Course Catalog" target="_blank">
                    	<div class="alignleft"><img src="//<?=JADE1_URL ?>/images/downloadCatalog2.png" alt="Download Course Catalog"></div>
						<h3>Download Our Course Catalog</h3>
						<p>
                        	JADE Learning offers nationwide continuing education in the form of online, homestudy, and classroom
                            courses.
                      	</p>
                 	</a>
				</article>
				<article>
                	<a href="//<?=JADE1_URL ?>/jadecc/registration/Continuing_Education_Course_Demos.php" title="Try a LIVE Demonstration of ANY Course">
						<div class="alignleft"><img src="//<?=JADE1_URL ?>/images/liveDemo2.png" alt="View Live Demo"></div>
						<h3>Try a LIVE Demonstration of ANY Course</h3>
                        <p>
                        	Our courses are free to start, but if you're not quite ready to register an account, try the demo
                            version. This will give you a feel for the look and format of our online courses.
                      	</p>
                	</a>
				</article>
                <article>
					<a href="//<?=JADE1_URL ?>/jadecc/registration/electricalHomestudyList.php" title="Download A Printable Homestudy">
                    	<div class="alignleft"><img src="//<?=JADE1_URL ?>/images/downloadHomestudy2.png" alt="Download Printable Homestudy"></div>
						<h3>Download A Printable Homestudy</h3>
                        <p>
                        	In a number of states, we have approved homestudy courses. You can open the PDF version, download it,
                            print it, work on it, and mail or fax us back your answer sheet.
                      	</p>
                	</a>
				</article>
			</section>
		</section>
		<aside>
			<section>
				<h3><span>Why Choose JADE Learning?</span></h3>
				<ul class="tick">
                    <li>Pay Only After You Pass!</li>
                    <li>Work at Your Own Pace</li>
										<li>Daily Rush-Reporting FREE!</li>
										<li>Excellent Customer Support</li>
                    <li>Instant Feedback and a Running Score</li>
                    <li>Instructor and Technical Support</li>
                    <li>See What Others Are Saying System</li>
										<li>Free Copies of All Testing Materials</li>
                    <li>Approved With <?=$activeST_nowCt?> Boards in <?=$activeS_nowCt?> States</li>
                    <li><a href="//<?=JADE1_URL ?>/newsletter/newsletterArchive.php" title="Continuing Education Newsletter" class="JADE_Learning">Free Professional Electrical CE Newsletter</a></li>
				</ul>
			</section>
			<section>
				<h3><span>Testimonials</span></h3>
                <a href="//<?=JADE1_URL ?>/jadecc/testimonials.php" title="JADE Learning Continuing Education Testimonials">
				<blockquote class="quote" style="max-width:100%;">
					<p>30 years in the business and I still learned something new.</p>
					<p class="signature">- <span>R. Hamilton</span> / Andrews, NC</p>
				</blockquote>
                </a>
                <a href="//<?=JADE1_URL ?>/jadecc/testimonials.php" title="JADE Learning Continuing Education Testimonials">
                <blockquote class="quote" style="margin-top:15px; max-width:100%;">
					<p>This course is excellent, it's convenient for people after work, and I was extremely satisfied with your customer service.</p>
					<p class="signature">- <span>J. Ayala</span> / Bakersfield, CA</p>
				</blockquote>
                </a>
			</section>
			<article>
				<h3><span>Upcoming Events</span></h3>
				<ul class="event-list">
                	<!-- Dynamic eventing here -->
                	<?=$eventList?>
				</ul>
			</article>
		</aside>

		<section class="content columns portfolio" style="padding:0;">
			<p class="more"><a href="//<?=JADE1_URL ?>/jadecc/view_courses.php" title=" View all of Our Continuing Education Courses">View all of our courses</a></p>
			<h2 style="margin-top:10px;"><span>Continuing Education Trades</span></h2>
			<article class="col4 ECE">
				<div class="img"><img src="//<?=JADE1_URL ?>/images/Electrical_CE.jpg" alt="Electrical CE">
					<div><ul><li><a href="//<?=JADE1_URL ?>" title="Electrical Continuing Education" class="action go">Go</a></li></ul></div>
				</div>
				<h3 style="color:#333"><a href="//<?=JADE1_URL ?>" title="Electrical Continuing Education">Electrical Continuing Education</a></h3>
				<p style="color:#666;">Course topics include the NEC changes, residential wiring, commercial and industrial wiring, services, grounding and bonding, overcurrent protection, electrical safety, green energy, and business.</p>
			</article><article class="col4 ACE">
				<div class="img"><img src="//<?=JADE1_URL ?>/images/Alarm_CE.jpg" alt="">
					<div><ul><li><a href="//<?=JADE1_URL ?>/Alarm_Continuing_Education.php" title="Alarm Continuing Education" class="action go">Go</a></li></ul></div>
				</div>
				<h3 style="color:#333"><a href="//<?=JADE1_URL ?>/Alarm_Continuing_Education.php" title="Alarm Continuing Education">Alarm Continuing Education</a></h3>
				<p style="color:#666;">Course topics include access control, CCTV, network security cameras, locks and strikes, and false alarms.</p>
			</article><article class="col4 EECE">
				<div class="img"><img src="//<?=JADE1_URL ?>/images/Engineer_CE.jpg" alt="">
					<div><ul><li><a href="//<?=JADE1_URL ?>/Electrical_Engineer_Continuing_Education.php" title="Electrical Engineer Continuing Education" class="action go">Go</a></li></ul></div>
				</div>
				<h3 style="color:#333"><a href="//<?=JADE1_URL ?>/Electrical_Engineer_Continuing_Education.php" title="Electrical Engineer Continuing Education">Electrical Engineer Continuing Education</a></h3>
				<p style="color:#666;">Course topics include the NEC changes, grounding and bonding, overcurrent protection, electrical safety, and lockout/tagout.</p>
			</article><article class="col4 EICE">
				<div class="img"><img src="//<?=JADE1_URL ?>/images/Inspector_CE.jpg" alt="">
					<div><ul><li><a href="//<?=JADE1_URL ?>/Electrical_Inspector_Continuing_Education.php" title="Electrical Inspector Continuing Education" class="action go">Go</a></li></ul></div>
				</div>
				<h3 style="color:#333"><a href="//<?=JADE1_URL ?>/Electrical_Inspector_Continuing_Education.php" title="Electrical Inspector Continuing Education">Electrical Inspector Continuing Education</a></h3>
				<p style="color:#666;">Course topics include the NEC changes, PV Systems, residential wiring, and residential electrical inspections.</p>
			</article>
		</section>
		<a href="#top" class="go-top">Go to top of page</a>
	</section>

	<?php require("jadecc/headers/footer.php"); ?>
    </div>

    <!-- If the access is coming from the app, create a similar navigational experience -->
	<?php
	if (isset($_SESSION['accss']) && $_SESSION['accss'] == "app")
	{
		include("sharedAssets/webslidemenu/appSiteMenuWrapBottom.inc.php");
		echo "<script type='text/javascript' src='//".JADE1_URL."/sharedAssets/webslidemenu/js/webslidemenu.js'></script>\n";
	}
    ?>

	<script type="text/javascript" src="//<?=JADE1_URL ?>/js/jquery.js"></script>
	<script type="text/javascript" src="//<?=JADE1_URL ?>/js/scripts.js"></script>
    <!-- Tooltips -->
    <script type="text/javascript" src="//<?=JADE1_URL ?>/js/LiteTooltip.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
			setFocus();
			Placeholders.init();
			$("#pwHint").LiteTooltip({ padding: 4, width: 260, opacity: 1, title: '<div class="template"><h4>Passwords</h4><p>Passwords are case sensitive.</p></div>' });
		});
	</script>

</body>
</html>
